Necessary POM Dependencies:

1. Spring Context - version 6.0.11
2. Spring Core - version 6.0.11
3. Spring Web - version 6.0.11
4. Spring Web MVC - version 6.0.11
5. Jakarta Servlet JSTL - version 3.0.1
6. Tomcat Jasper - version 11.0.0-M10
7. Javax Servlet - version 3.0-alpha-1
8. JSTL - version 1.2
9. My Sql Connector version 8.1.0
10. Microsoft Sql - version 12.2.0
11. Spring JDBC - version 6.0.11
12. Spring Expression Language - version 6.0.11
13. Standard Tags Library - version 1.2.5
14. Apache Taglibs - version 1.2.5
15. jakarta servlet - version 3.0.0
16. javax.servlet - version>4.0.1